%%
clear all
close all
clc

[z,p,k] = butter(5,100,'low','s');

filter = zpk(z,p,k);

bode(filter)

% go to state space
statespace = canon(filter,'companion');

% get the abcd matrices
[a,b,c,d] = ssdata(statespace);



