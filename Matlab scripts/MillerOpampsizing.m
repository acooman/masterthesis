%%ontwerp opamp thesis


%% Process parameters

n=1.3;
KPMOS=93.9;
KNMOS=258;

%% Specs

GBW=200e6;
Cl=5e-12;

%% Design

a=3;
b=3;
g=3;

%% input transistor M1

M1.gm=Cl*2*pi*GBW/a;
M1.Vov=0.2;
M1.gmoverId=2/M1.Vov;
M1.Id=M1.gm/M1.gmoverId;
M1.WoL=2*1e6*n*M1.Id/KPMOS/M1.Vov/M1.Vov;

%% differential tail transistor M5

M5.Id=2*M1.Id;
M5.Vov=0.2;
M5.WoL=2*1e6*n*M5.Id/KPMOS/M5.Vov/M5.Vov;

%% output transistor M2

M2.gm=2*pi*g*GBW*Cl*(1+1/b);
M2.Vov=0.2;
M2.gmoverId=2/M2.Vov;
M2.Id=M2.gm/M2.gmoverId;
M2.WoL=2*1e6*n*M2.Id/KNMOS/M2.Vov/M2.Vov;

%% output load transistor M4

M4.Id=M2.Id;
M4.Vov=0.2;
M4.WoL=2*1e6*n*M4.Id/KPMOS/M4.Vov/M4.Vov;

%% mirror transistor M3

M3.Vov=0.2;
M3.Id=M1.Id;
M3.WoL=2*1e6*n*M3.Id/KNMOS/M3.Vov/M3.Vov;


